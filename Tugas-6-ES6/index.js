//soal 1
const p = 5; 
const l = 10;

function luas() {
    console.log(p*l);
}

luas ()

function keliling() {
    console.log(2*(p*l));
}

keliling ()

//soal 2 
  const newFunction = (firstName, lastName) => {
      return{
        firstName,
        lastName,
        fullName(){
            console.log(firstName + " " + lastName)
            return
        }
    }
  }
  newFunction("William", "Imoh").fullName()
 
//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName,address, hobby} = newObject

console.log(firstName, lastName, address, hobby)

//soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east]
console.log(combined)

//soal 5
const planet = "earth" 
const view = "glass" 
const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)