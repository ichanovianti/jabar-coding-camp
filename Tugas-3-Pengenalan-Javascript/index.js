//soal 1
var pertama = "saya sangat senang hari ini"
var kedua = " belajar javascript itu keren"
//Jawaban soal 1
var word1 = pertama.substring(0,4)
var word2 = pertama.substring(11,18)
var word3 = kedua.substring(0,9)
var word4 = kedua.substring(9,19)
var upper  = word4.toUpperCase()

console.log(word1.concat(word2,word3,upper))

//soal 2 
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
//Jawaban soal 2
var int1 = 10
var int2 = 2
var int3 = 4
var int4 = 6 
var operasi = (int2*int3)+(int1+int4)

console.log(operasi)

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
//jawaban
var kataKedua = kalimat.substring(3,14)
var kataKetiga = kalimat.substring(14,18)
var kataKeempat = kalimat.substring(18,24)
var kataKelima = kalimat.substring(24,31)

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
