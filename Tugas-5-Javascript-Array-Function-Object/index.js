//soal 1 
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort()
console.log(daftarHewan)

for (var i = 1; i < daftarHewan.length; i++)
    for (var j = 0; j < i; j++)
        if (daftarHewan[i] < daftarHewan[j]) {
          var x = daftarHewan[i];
          daftarHewan[i] = daftarHewan[j];
          daftarHewan[j] = x;
        }

console.log(daftarHewan);

//soal 2
 
    function introduce (data) {
        console.log('Nama saya ' + data.name + 
        ', umur saya ' + data.age +
        ', alamat saya di ' + data.address + 
        ', dan saya punya hobi yaitu ' + data.hobby);
    }
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) 


//soal 3

var vokal = ["a","A","i","I","u","U","e","E","o","O"]

function hitung_huruf_vokal(str){
    var hitung = 0
    for (var letter of str.toLowerCase()) {
        if (vokal.includes(letter)) {
            hitung++;
        }
    }
    return hitung
      }

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

//soal 4

function hitung (int) {
    var hasil;
    hasil = int*2-2;
    return hasil;
}

console.log( hitung(0) ) 
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) )